package com.tsc.skuschenko.tm.exception;

public class AbstractException extends RuntimeException {

    public AbstractException(String value) {
        super(value);
    }

}
